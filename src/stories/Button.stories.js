import Test from "../components/Test/Test"
import React from "react";

export default {
    title: 'Component/Button',
    component: Test,
    argTypes: {
        background: { control: 'color' },
        color: { control: 'color' },
        padding: {control: 'number', defaultValue: 5} 
    }
};
export const Red = () => (<Test/>)